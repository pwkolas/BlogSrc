library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ReadIntFromFile is
end ReadIntFromFile;

architecture ReadIntFromFile_rtl of ReadIntFromFile is

   constant C_FILE_NAME_RD :string  := "./dat/ReadIntFromFileIn.dat";
   constant C_FILE_NAME_WR :string  := "./dat/ReadIntFromFileOut.dat";
   constant C_CLK          :time := 10 ns;
   
   signal clk              :std_logic := '0';
   signal rst              :std_logic := '0';
   signal data             :integer := 1;
   signal eof              :std_logic := '0';

   type INTEGER_FILE is file of integer;
   file fptrrd             :INTEGER_FILE;
   file fptrwr             :INTEGER_FILE;

begin

ClockGenerator: process
begin
   clk <= '0' after C_CLK, '1' after 2*C_CLK;
   wait for 2*C_CLK;
end process;

rst <= '1', '0' after 100 ns;

GetData_proc: process

   variable statrd : FILE_OPEN_STATUS;
   variable statwr : FILE_OPEN_STATUS;

   variable varint_data    :integer := 1;

begin

   data      <= 1;
   eof       <= '0';

   wait until rst = '0';

   file_open(statrd, fptrrd, C_FILE_NAME_RD, read_mode);
   file_open(statwr, fptrwr, C_FILE_NAME_WR, write_mode);

   while (not endfile(fptrrd)) loop
      wait until clk = '1';
      read(fptrrd, varint_data);
      write(fptrwr, varint_data);
      data  <= varint_data;
   end loop;
   wait until rising_edge(clk);
   eof       <= '1';
   file_close(fptrrd);
   file_close(fptrwr);
   wait;
end process;

end ReadIntFromFile_rtl;