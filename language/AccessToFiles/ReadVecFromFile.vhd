library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ReadVecFromFile is
end ReadVecFromFile;

architecture ReadVecFromFile_rtl of ReadVecFromFile is

   constant C_FILE_NAME_RD :string  := "./dat/ReadVecFromFileIn.dat";
   constant C_FILE_NAME_WR :string  := "./dat/ReadVecFromFileOut.dat";
   constant C_CLK          :time := 10 ns;

   signal clk              :std_logic := '0';
   signal rst              :std_logic := '0';
   signal data             :std_logic_vector(4-1 downto 0);
   signal eof              :std_logic := '0';

   type STD_FILE is file of std_logic_vector(4-1 downto 0);
   file fptrrd             :STD_FILE;
   file fptrwr             :STD_FILE;

begin

ClockGenerator: process
begin
   clk <= '0' after C_CLK, '1' after 2*C_CLK;
   wait for 2*C_CLK;
end process;

rst <= '1', '0' after 100 ns;

GetData_proc: process

   variable statrd         :FILE_OPEN_STATUS;
   variable statwr         :FILE_OPEN_STATUS;

   variable varstd_data    :std_logic_vector(4-1 downto 0);

begin

   data      <= (others => '0');
   eof       <= '0';

   wait until rst = '0';

   file_open(statrd, fptrrd, C_FILE_NAME_RD, read_mode);
   file_open(statwr, fptrwr, C_FILE_NAME_WR, write_mode);

   while (not endfile(fptrrd)) loop
      wait until clk = '1';
      read(fptrrd, varstd_data);
      write(fptrwr, varstd_data);
      data <= varstd_data;
   end loop;
   wait until rising_edge(clk);
   eof       <= '1';
   file_close(fptrrd);
   file_close(fptrwr);
   wait;
end process;

end ReadVecFromFile_rtl;