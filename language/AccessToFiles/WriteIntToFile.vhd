library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity WriteIntToFile is
end WriteIntToFile;

architecture WriteIntToFile_rtl of WriteIntToFile is

   constant C_FILE_NAME_RD :string  := "./dat/WriteIntToFile.dat";
   constant C_FILE_NAME_WR :string  := "./dat/WriteIntToFile.dat";
   constant C_CLK          :time    := 10 ns;

   signal clk              :std_logic := '0';
   signal rst              :std_logic := '0';
   signal data             :integer := 1;
   signal eowr             :std_logic := '0';
   signal eord             :std_logic := '0';

   type INTEGER_FILE is file of integer;
   file fptrrd             :INTEGER_FILE;
   file fptrwr             :INTEGER_FILE;

begin

ClockGenerator: process
begin
   clk <= '0' after C_CLK, '1' after 2*C_CLK;
   wait for 2*C_CLK;
end process;

rst <= '1', '0' after 100 ns;

GetData_proc: process

   variable statrd : FILE_OPEN_STATUS;
   variable statwr : FILE_OPEN_STATUS;

   variable varint_datawr    :integer := 1;
   variable varint_datard    :integer := 1;

begin

   data      <= 1;
   eowr      <= '0';
   eord      <= '0';

   wait until rst = '0';

   -- write to file
   file_open(statwr, fptrwr, C_FILE_NAME_WR, write_mode);
   while varint_datawr < 100 loop
      wait until clk = '1';
      varint_datawr := varint_datawr + 10;
      write(fptrwr, varint_datawr);
   end loop;   
   file_close(fptrwr);
   wait until rising_edge(clk);
   eowr       <= '1';

   -- read from file
   file_open(statrd, fptrrd, C_FILE_NAME_RD, read_mode);
   while (not endfile(fptrrd)) loop
      wait until clk = '1';
      read(fptrrd, varint_datard);
      data <= varint_datard;
   end loop;   
   file_close(fptrrd);
   wait until rising_edge(clk);
   eord       <= '1';

   wait;
end process;

end WriteIntToFile_rtl;