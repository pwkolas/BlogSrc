library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity WriteVecToFile is
end WriteVecToFile;

architecture WriteVecToFile_rtl of WriteVecToFile is

   constant C_FILE_NAME_RD :string  := "./dat/WriteVecToFile.dat";
   constant C_FILE_NAME_WR :string  := "./dat/WriteVecToFile.dat";
   constant C_CLK          :time    := 10 ns;

   signal clk              :std_logic := '0';
   signal rst              :std_logic := '0';
   signal data             :std_logic_vector(4-1 downto 0);
   signal eowr             :std_logic := '0';
   signal eord             :std_logic := '0';

   type STD_FILE is file of std_logic_vector(4-1 downto 0);
   file fptrrd             :STD_FILE;
   file fptrwr             :STD_FILE;

begin

ClockGenerator: process
begin
   clk <= '0' after C_CLK, '1' after 2*C_CLK;
   wait for 2*C_CLK;
end process;

rst <= '1', '0' after 100 ns;

GetData_proc: process

   variable statrd         :FILE_OPEN_STATUS;
   variable statwr         :FILE_OPEN_STATUS;

   variable varint_datawr  :integer := 0;
   variable varstd_datawr  :std_logic_vector(4-1 downto 0) := (others => '0');
   variable varstd_datard  :std_logic_vector(4-1 downto 0) := (others => '0');

begin

   data      <= (others => '0');
   eowr      <= '0';
   eord      <= '0';

   wait until rst = '0';

   -- write to file
   file_open(statwr, fptrwr, C_FILE_NAME_WR, write_mode);
   while varint_datawr < 10 loop
      wait until clk = '1';
      varint_datawr := varint_datawr + 1;
      varstd_datawr := std_logic_vector(to_unsigned(varint_datawr, 4));
      write(fptrwr, varstd_datawr);
   end loop;
   wait until rising_edge(clk);
   eowr       <= '1';   
   file_close(fptrwr);

   -- read from file
   file_open(statrd, fptrrd, C_FILE_NAME_RD, read_mode);
   while (not endfile(fptrrd)) loop
      wait until clk = '1';
      read(fptrrd, varstd_datard);
      data <= varstd_datard;
   end loop;
   wait until rising_edge(clk);
   eord       <= '1';
   file_close(fptrrd);

   wait;
end process;

end WriteVecToFile_rtl;