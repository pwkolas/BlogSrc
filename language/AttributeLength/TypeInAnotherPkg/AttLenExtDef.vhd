-- PACKAGE
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

PACKAGE pkg IS
   CONSTANT c_TestVec :std_logic_vector(12-1 DOWNTO 0) := x"100";
   TYPE TypeA IS ARRAY (1 TO 3) OF unsigned(16-1 DOWNTO 0);
END PACKAGE pkg;

PACKAGE BODY pkg IS
END PACKAGE BODY pkg;

-- TESTED ENTITY
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.pkg.ALL;

ENTITY AttLenExtDef IS
   PORT (
      clk : IN STD_LOGIC;
      rst : IN STD_LOGIC;

      Aarr : IN  TypeA;
      Barr : IN  TypeA;
      Yarr : OUT TypeA;

      Avec : IN STD_LOGIC_VECTOR(c_TestVec'LENGTH-1 DOWNTO 0);
      Yvec : OUT STD_LOGIC_VECTOR(c_TestVec'LENGTH-1 DOWNTO 0)
      );
END AttLenExtDef;

ARCHITECTURE rtl OF AttLenExtDef IS

BEGIN
   -- Yarr(1) = Aarr(1) + Barr(1)
   -- Yarr(2) = Aarr(2) + Barr(2)
   -- Yarr(3) = Aarr(3) + Barr(3)
   -- Yvec    = Avec;
   calc : PROCESS(clk)
   BEGIN
      IF rising_edge(clk) THEN
         IF rst = '1' THEN
            Yarr <= (OTHERS => (OTHERS => '0'));
            Yvec <= (OTHERS => '0');
         ELSE
            FOR i IN 1 TO TypeA'LENGTH LOOP
               Yarr(i) <= Aarr(i) + Barr(i);
            END LOOP;

            Yvec <= Avec;
         END IF;
      END IF;
   END PROCESS;

END ARCHITECTURE;
