LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.pkg.ALL;

ENTITY AttLenExtDef_tb IS
END AttLenExtDef_tb;

ARCHITECTURE AttLenExtDef_tb_rtl OF AttLenExtDef_tb IS

   COMPONENT AttLenExtDef IS
      PORT (
         clk : IN STD_LOGIC;
         rst : IN STD_LOGIC;

         Aarr : IN  TypeA;
         Barr : IN  TypeA;
         Yarr : OUT TypeA;

         Avec : IN  STD_LOGIC_VECTOR(c_TestVec'LENGTH-1 DOWNTO 0);
         Yvec : OUT STD_LOGIC_VECTOR(c_TestVec'LENGTH-1 DOWNTO 0)
         );
   END COMPONENT;

   SIGNAL clk : STD_LOGIC;
   SIGNAL rst : STD_LOGIC;

   SIGNAL Aarr : TypeA;
   SIGNAL Barr : TypeA;
   SIGNAL Yarr : TypeA;

   SIGNAL Avec : STD_LOGIC_VECTOR(c_TestVec'LENGTH-1 DOWNTO 0);
   SIGNAL Yvec : STD_LOGIC_VECTOR(c_TestVec'LENGTH-1 DOWNTO 0);

   CONSTANT ClkGenConst : TIME := 20 NS;

BEGIN

   ClockGenerator : PROCESS
   BEGIN
      clk <= '0' AFTER ClkGenConst, '1' AFTER 2*ClkGenConst;
      WAIT FOR 2*ClkGenConst;
   END PROCESS;

   UUT : AttLenExtDef
      PORT MAP (
         clk  => clk,
         rst  => rst,
         Aarr => Aarr,
         Barr => Barr,
         Yarr => Yarr,
         Avec => Avec,
         Yvec => Yvec
         );

   rst  <= '1', '0' AFTER 60 NS;
           
   Aarr(1) <= x"1001", x"2001" AFTER 100 NS;
   Aarr(2) <= x"1002", x"2002" AFTER 100 NS;
   Aarr(3) <= x"1003", x"2003" AFTER 100 NS;
   Barr(1) <= x"3001", x"6001" AFTER 160 NS;
   Barr(2) <= x"3002", x"6002" AFTER 160 NS;
   Barr(3) <= x"3003", x"6003" AFTER 160 NS;

   Avec <= x"001", x"002" AFTER 100 NS, x"003" AFTER 300 NS;
   
END AttLenExtDef_tb_rtl;
