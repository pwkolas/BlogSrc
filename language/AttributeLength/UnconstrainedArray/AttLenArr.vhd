-- PACKAGE
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

PACKAGE pkg IS
   TYPE TypeB IS ARRAY (NATURAL RANGE <>) OF UNSIGNED(16-1 DOWNTO 0);
END PACKAGE pkg;

PACKAGE BODY pkg IS
END PACKAGE BODY pkg;

-- TESTED ENTITY
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.pkg.ALL;

ENTITY AttLenArr IS
   PORT (
      clk : IN STD_LOGIC;
      rst : IN STD_LOGIC;

      Aarr : IN  TypeB;
      Barr : IN  TypeB;
      Yarr : OUT TypeB
      );
END AttLenArr;

ARCHITECTURE rtl OF AttLenArr IS

BEGIN
   calc : PROCESS(clk)
   BEGIN
      IF rising_edge(clk) THEN
         IF rst = '1' THEN
            FOR i IN 1 TO Yarr'LENGTH LOOP
               Yarr(i) <= (OTHERS => '0');
            END LOOP;            
         ELSE
            FOR i IN 1 TO Yarr'LENGTH LOOP
               Yarr(i) <= Aarr(i) + Barr(i);
            END LOOP;
         END IF;
      END IF;
   END PROCESS;

END ARCHITECTURE;
