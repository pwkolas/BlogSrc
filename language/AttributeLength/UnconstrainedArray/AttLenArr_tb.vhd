LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.pkg.ALL;

ENTITY AttLenArr_tb IS
END AttLenArr_tb;

ARCHITECTURE AttLenArr_tb_rtl OF AttLenArr_tb IS

   COMPONENT AttLenArr IS
      PORT (
         clk  : IN STD_LOGIC;
         rst  : IN STD_LOGIC;

         Aarr : IN  TypeB;
         Barr : IN  TypeB;
         Yarr : OUT TypeB
         );
   END COMPONENT;

   SIGNAL clk : STD_LOGIC;
   SIGNAL rst : STD_LOGIC;

   SIGNAL Aarr : TypeB(4 DOWNTO 1);
   SIGNAL Barr : TypeB(4 DOWNTO 1);
   SIGNAL Yarr : TypeB(4 DOWNTO 1);

   CONSTANT ClkGenConst : TIME := 20 NS;

BEGIN

   ClockGenerator : PROCESS
   BEGIN
      clk <= '0' AFTER ClkGenConst, '1' AFTER 2*ClkGenConst;
      WAIT FOR 2*ClkGenConst;
   END PROCESS;

   UUT : AttLenArr
      PORT MAP (
         clk  => clk,
         rst  => rst,
         Aarr => Aarr,
         Barr => Barr,
         Yarr => Yarr
         );

   rst <= '1', '0' AFTER 60 NS;

   Aarr(1) <= x"1001", x"2001" AFTER 100 NS;
   Aarr(2) <= x"1002", x"2002" AFTER 100 NS;   
   Aarr(3) <= x"1003", x"2003" AFTER 100 NS;
   Aarr(4) <= x"1004", x"2004" AFTER 100 NS;
   Barr(1) <= x"3001", x"6001" AFTER 160 NS;
   Barr(2) <= x"3002", x"6002" AFTER 160 NS;
   Barr(3) <= x"3003", x"6003" AFTER 160 NS;
   Barr(4) <= x"3004", x"6004" AFTER 160 NS;

END AttLenArr_tb_rtl;
