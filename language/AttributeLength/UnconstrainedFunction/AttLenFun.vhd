-- TESTED ENTITY
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY AttLenFun IS
   PORT (
      A : IN  unsigned(8-1 downto 0);
      B : IN  unsigned(8-1 downto 0);
      C : IN  unsigned(8-1 downto 0);
      Y : OUT unsigned(8-1 downto 0)
      );
END AttLenFun;

ARCHITECTURE rtl OF AttLenFun IS

-- a, b, c - the same length
FUNCTION Add3 (a, b, c : unsigned) return unsigned is
	variable y :unsigned(a'length-1 downto 0);
begin
   y := a + b + c;
   return y;
end Add3;

BEGIN
   Y <= Add3(A, B, C);
END ARCHITECTURE;
