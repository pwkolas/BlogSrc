LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY AttLenFun_tb IS
END AttLenFun_tb;

ARCHITECTURE AttLenFun_tb_rtl OF AttLenFun_tb IS

COMPONENT AttLenFun IS
   PORT (
      A : IN  unsigned(8-1 downto 0);
      B : IN  unsigned(8-1 downto 0);
      C : IN  unsigned(8-1 downto 0);
      Y : OUT unsigned(8-1 downto 0)
   );
END COMPONENT;

   signal A : unsigned(8-1 downto 0);
   signal B : unsigned(8-1 downto 0);
   signal C : unsigned(8-1 downto 0);
   signal Y : unsigned(8-1 downto 0);

BEGIN

   UUT : AttLenFun
      PORT MAP (
         A => A,
         B => B,
         C => C,
         Y => Y
      );

   A <= x"01", x"02" AFTER 100 NS;
   B <= x"03", x"04" AFTER 160 NS;
   C <= x"05", x"06" AFTER 200 NS;

END AttLenFun_tb_rtl;
