library ieee;
use ieee.std_logic_1164.all;
 
entity tb_dff is
end entity;

architecture tb_dff_rtl of tb_dff is

component dff is
  port (
    clk  :in std_logic;
    d    :in std_logic;
    q    :out std_logic
  );
end component;

signal clk  :std_logic;
signal d    :std_logic;
signal q    :std_logic;

constant ClkGenConst  :time := 10 ns;

begin

UUT: dff
  port map (
    clk => clk,
    d   => d,
    q   => q
);

ClockGenerator: process
begin
  clk <= '0' after ClkGenConst, '1' after 2*ClkGenConst;
  wait for 2*ClkGenConst;
end process;

  d <= '1', '0' after 105 ns, '1' after 205 ns;

end architecture;