library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;

entity ReadDataFromFile is
end ReadDataFromFile;

architecture ReadDataFromFile_rtl of ReadDataFromFile is

   constant C_FILE_NAME :string  := "DataIn.dat";
   constant C_DATA1_W   :integer := 16;
   constant C_DATA3_W   :integer :=  4;
   constant C_CLK       :time    := 10 ns;

   signal clk           :std_logic := '0';
   signal rst           :std_logic := '0';
   signal data1         :std_logic_vector(C_DATA1_W-1 downto 0);
   signal data2         :integer;
   signal data3         :std_logic_vector(C_DATA3_W-1 downto 0);
   signal eof           :std_logic := '0';

   file fptr: text;

begin

ClockGenerator: process
begin
   clk <= '0' after C_CLK, '1' after 2*C_CLK;
   wait for 2*C_CLK;
end process;

rst <= '1', '0' after 100 ns;

GetData_proc: process

   variable fstatus       :file_open_status;
   
   variable file_line     :line;
   variable var_data1     :std_logic_vector(C_DATA1_W-1 downto 0);
   variable var_data2     :integer;
   variable var_data3     :std_logic_vector(C_DATA3_W-1 downto 0);
      
begin

   data1     <= (others => '0');
   var_data1 := (others => '0');
   data2     <= 0;
   var_data2 := 0;
   data3     <= (others => '0');
   var_data3 := (others => '0');
   eof       <= '0';

   wait until rst = '0';

   file_open(fstatus, fptr, C_FILE_NAME, read_mode);

   while (not endfile(fptr)) loop
      wait until clk = '1';
      readline(fptr, file_line);
      hread(file_line, var_data1);
      data1      <= var_data1;
      read(file_line, var_data2);
      data2      <= var_data2;
      read(file_line, var_data3);
      data3      <= var_data3;
   end loop;
   wait until rising_edge(clk);
   eof       <= '1';
   file_close(fptr);
   wait;
end process;

end ReadDataFromFile_rtl;
