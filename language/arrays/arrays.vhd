library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package arr_pkg is

   constant C_PKG_ADC_CH_NUM  : natural :=  4;
   constant C_PKG_ADC_WIDTH   : natural := 12;
   type t_arr_AdcData is array (0 to C_PKG_ADC_CH_NUM-1) of std_logic_vector(C_PKG_ADC_WIDTH-1 downto 0);

end arr_pkg;

-- BODY -- 
package body arr_pkg is
end arr_pkg;

-- dummy block
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.arr_pkg.all;

entity DummyBlock is
   generic (
      G_WIDTH  : natural := C_PKG_ADC_WIDTH
   );
   port (
      clk	: in std_logic;
      rst	: in std_logic;

      iv_data	: in std_logic_vector(G_WIDTH-1 downto 0);
      ov_data	: out std_logic_vector(G_WIDTH-1 downto 0)
   );
end DummyBlock;

architecture DummyBlock_bhv of DummyBlock is 

begin

   -- we can do here some nice stuff with data
   -- but now we will just send input to output
   -- assign inputs to "array cells"
   ov_data  <= iv_data;

end DummyBlock_bhv;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.arr_pkg.all;

entity arrays is
   generic (
      C_ADC_CH_NUM   : natural := C_PKG_ADC_CH_NUM;
      G_WIDTH        : natural := C_PKG_ADC_WIDTH
   );
   port (
      clk	: in std_logic;
      rst	: in std_logic;

      iv_adc0	: in std_logic_vector(G_WIDTH-1 downto 0);
      iv_adc1	: in std_logic_vector(G_WIDTH-1 downto 0);
      iv_adc2	: in std_logic_vector(G_WIDTH-1 downto 0);
      iv_adc3 	: in std_logic_vector(G_WIDTH-1 downto 0);

      ov_data0	: out std_logic_vector(G_WIDTH-1 downto 0);
      ov_data1	: out std_logic_vector(G_WIDTH-1 downto 0);
      ov_data2	: out std_logic_vector(G_WIDTH-1 downto 0);
      ov_data3	: out std_logic_vector(G_WIDTH-1 downto 0)
   );
end arrays;

architecture arrays_bhv of arrays is 

   signal arr_AdcData	: t_arr_AdcData;
   signal arr_DB_data   : t_arr_AdcData;

begin
   
   -- assign inputs to "array cells"
   arr_AdcData(0) <= iv_adc0;
   arr_AdcData(1) <= iv_adc1;
   arr_AdcData(2) <= iv_adc2;
   arr_AdcData(3) <= iv_adc3;
   
   -- send data through blocks
   -- for every "array cell" exists another instantiation of DummyBlock
   GEN_DB: for I in 0 to C_ADC_CH_NUM-1 generate
      DummyBlock_inst : entity work.DummyBlock  
         port map (
            clk	   => clk,
            rst	   => rst,
            iv_data  => arr_AdcData(I),
            ov_data  => arr_DB_data(I)
         );
   end generate GEN_DB;
   
   ov_data0 <= arr_AdcData(0);
   ov_data1	<= arr_AdcData(1);
   ov_data2	<= arr_AdcData(2);
   ov_data3	<= arr_AdcData(3);

end arrays_bhv;