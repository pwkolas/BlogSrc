library ieee;
use ieee.std_logic_1164.all;
use work.arr_pkg.all;
 
entity tb_arrays is
end entity;

architecture tb_arrays_rtl of tb_arrays is

component arrays is
   generic (
      C_ADC_CH_NUM   : natural := C_PKG_ADC_CH_NUM;
      G_WIDTH        : natural := C_PKG_ADC_WIDTH
   );
   port (
      clk	: in std_logic;
      rst	: in std_logic;

      iv_adc0	: in std_logic_vector(G_WIDTH-1 downto 0);
      iv_adc1	: in std_logic_vector(G_WIDTH-1 downto 0);
      iv_adc2	: in std_logic_vector(G_WIDTH-1 downto 0);
      iv_adc3 	: in std_logic_vector(G_WIDTH-1 downto 0);

      ov_data0	: out std_logic_vector(G_WIDTH-1 downto 0);
      ov_data1	: out std_logic_vector(G_WIDTH-1 downto 0);
      ov_data2	: out std_logic_vector(G_WIDTH-1 downto 0);
      ov_data3	: out std_logic_vector(G_WIDTH-1 downto 0)
   );
end component;

   signal clk         : std_logic;
   signal rst         : std_logic;
   signal iv_adc0     : std_logic_vector(C_PKG_ADC_WIDTH-1 downto 0);
   signal iv_adc1	    : std_logic_vector(C_PKG_ADC_WIDTH-1 downto 0);
   signal iv_adc2	    : std_logic_vector(C_PKG_ADC_WIDTH-1 downto 0);
   signal iv_adc3	    : std_logic_vector(C_PKG_ADC_WIDTH-1 downto 0);
   signal ov_data0    : std_logic_vector(C_PKG_ADC_WIDTH-1 downto 0);
   signal ov_data1	 : std_logic_vector(C_PKG_ADC_WIDTH-1 downto 0);
   signal ov_data2	 : std_logic_vector(C_PKG_ADC_WIDTH-1 downto 0);
   signal ov_data3	 : std_logic_vector(C_PKG_ADC_WIDTH-1 downto 0);

constant ClkGenConst  :time := 10 ns;

begin

ClockGenerator: process
begin
   clk <= '0' after ClkGenConst, '1' after 2*ClkGenConst;
   wait for 2*ClkGenConst;
end process;

arrays_inst: arrays
   generic map (
      G_WIDTH  => C_PKG_ADC_WIDTH
   )
   port map (
      clk   => clk,
      rst	=> rst,
      iv_adc0	=> iv_adc0,
      iv_adc1	=> iv_adc1,
      iv_adc2	=> iv_adc2,
      iv_adc3	=> iv_adc3,
      ov_data0	=> ov_data0,
      ov_data1	=> ov_data1,
      ov_data2	=> ov_data2,
      ov_data3	=> ov_data3
   );

	rst  <= '1', '0' after 100 ns;
	iv_adc0 <= x"AAA";
   iv_adc1 <= x"BBB";
   iv_adc2 <= x"CCC";
   iv_adc3 <= x"DDD";

end architecture;