library ieee;
use ieee.std_logic_1164.all;

entity OrGate is
  port (	
    a	:in std_logic;
    b	:in std_logic;
    y	:out std_logic
  );
end OrGate;

architecture rtl of OrGate is
begin
  y <= a or b;
end architecture;