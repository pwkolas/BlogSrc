library ieee;
use ieee.std_logic_1164.all;

entity OrGate_tb is
end OrGate_tb;

architecture OrGate_tb_rtl of OrGate_tb is

  component OrGate is
    port (	
      a	:in std_logic;
      b	:in std_logic;
      y	:out std_logic
    );
  end component;

  signal a_TB, b_TB 	:std_logic;
  signal y_TB 		:std_logic;

begin
UUT: OrGate
  port map (
    a	=> a_TB,
    b	=> b_TB,
    y	=> y_TB
  );

  a_TB <= '1', '0' after 100 ns, '1' after 300 ns;
  b_TB <= '0', '1' after 200 ns;

end OrGate_tb_rtl;