library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity generics is
   generic (
      G_N    : integer := 8;
      G_DIR  : string  := "UP"
   );
   port (
      clk      : in std_logic;
      rst      : in std_logic;
      q        : out std_logic_vector(G_N-1 downto 0)
   );
end generics;

architecture generics_rtl of generics is

   signal main_cnt	: unsigned(G_N-1 downto 0);

begin

   counter_p: process(clk)
   begin
      if rising_edge(clk) then
         if rst = '1' then
            main_cnt <= (others => '0');
         else
            if G_DIR = "UP" then
               main_cnt <= main_cnt + 1;
            elsif G_DIR = "DOWN" then
               main_cnt <= main_cnt - 1;
            else
               main_cnt <= main_cnt + 1;
            end if;
         end if;
      end if;
   end process counter_p;

   q <= std_logic_vector(main_cnt);

end architecture generics_rtl;
