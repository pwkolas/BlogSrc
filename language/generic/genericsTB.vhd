library ieee;
use ieee.std_logic_1164.all;

entity genericsTB is
   generic (
      G_N       :integer := 8;
      G_DIR     :string  := "UP"
   );
end entity;

architecture genericsTBBhv of genericsTB is

   component generics is
      generic (
         G_N       : integer := 8;
         G_DIR     : string  := "UP"
      );
      port (
         clk      : in std_logic;
         rst      : in std_logic;
         q        : out std_logic_vector(G_N-1 downto 0)
      );
   end component;

   signal clk     : std_logic;
   signal rst     : std_logic;
   signal q       : std_logic_vector(G_N-1 downto 0);

   constant ClkGenConst : time := 10 ns;

begin

   generics_comp: generics
      generic map (
         G_N       => G_N,
         G_DIR     => G_DIR
      )
      port map (
         clk   => clk,
         rst   => rst,
         q     => q
      );

   ClockGenerator: process
   begin
      clk <= '0' after ClkGenConst, '1' after 2*ClkGenConst;
      wait for 2*ClkGenConst;
   end process;

   rst <= '1', '0' after 90 ns;

end architecture;
