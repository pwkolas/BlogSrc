library ieee;
use ieee.std_logic_1164.all;
 
entity WaitTests is
port (
   clka     :in std_logic;
   clkb     :in std_logic;
   a        :in std_logic;
   b        :in std_logic;
   oa       :out std_logic;
   ob       :out std_logic
);
end entity;

architecture WaitTests_rtl of WaitTests is

begin

WaitUntil: process
begin
   wait until rising_edge(clka);
   oa <= a;
   wait until clka = '1';
   oa <= b;
end process;

end architecture;