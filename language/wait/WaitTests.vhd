library ieee;
use ieee.std_logic_1164.all;
 
entity WaitTests is
end entity;

architecture WaitTests_rtl of WaitTests is

   signal clk      :std_logic;
   signal a        :std_logic := '1';
   signal b        :std_logic := '0';
   signal oa       :std_logic := '0';
   signal ob       :std_logic := '0';

constant ClkGenConst  :time := 10 ns;

begin

WaitFor: process
begin
  clk <= '0' after ClkGenConst, '1' after 2*ClkGenConst;
  wait for 2*ClkGenConst;
end process;

WaitUntil: process
begin
   wait until rising_edge(a);
   oa <= '1';
end process;

WaitOn: process
begin
   wait on b;
   ob <= '1';
   wait on b;
   ob <= '0';
   wait until falling_edge(b);
   ob <= '1';
end process;

run_proc: process
begin

   wait for 100 ns;
      a   <= '0';
      b   <= '1';
      
   wait for 100 ns;
      a   <= '1';
      b   <= '0';

   wait for 100 ns;
      b   <= '1';

   wait for 100 ns;
      b   <= '0';

   wait;
end process;

end architecture;