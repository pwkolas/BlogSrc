function FilRes = AvgFilter(DataIn, FilLen)
   VecSize = size(DataIn);
   TmpRes = (zeros(VecSize));
   %for first samples, when nrOfSample < FileLen
   for i = 1:FilLen-1
      acc = 0;
      for j = 1:i
         acc = acc + DataIn(j);
      end
      acc = acc/FilLen;
      TmpRes(i) = fix(acc);
   end
   %for next samples, when nrOfSample >= FileLen
   for i = (FilLen):VecSize(1)
      acc = 0;
      for j = 0:FilLen-1
         acc = acc + DataIn(i-j);
      end
      acc = acc/FilLen;
      TmpRes(i) = fix(acc);
   end   
   FilRes = TmpRes;
end