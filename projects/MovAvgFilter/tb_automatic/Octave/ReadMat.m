function VecOut = ReadMat(FileName, ff)
   fid = fopen(FileName, 'r');
   VecOut = fscanf(fid, ff);
   fclose(fid);
end