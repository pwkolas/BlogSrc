clear;

SimRes = ReadMat('DataOut.dat', '%x');
OctRes = ReadMat('DataOut_Octave.dat', '%x');

SimLen = size(SimRes);
OctLen = size(OctRes);

CompRes = isequal(SimRes, OctRes);

if CompRes
   disp('Comparison OK!');
else
   disp('Comparison NOT OK!');
end