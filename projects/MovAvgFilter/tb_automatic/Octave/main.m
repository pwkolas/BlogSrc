clear;
addpath ../../src/

DataInLen = 10;
CellMinVal = 100;
CellMaxVal = 1000;

FilLen = 4;

DataIn = randi([CellMinVal, CellMaxVal], DataInLen, 1);
WriteMat(DataIn, "DataIn.dat", '%04X');

DataOut = AvgFilter(DataIn, FilLen);

WriteMat(DataOut, "DataOut_Octave.dat", '%04X');