library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
 
entity ReadDataFromFile is
   generic (
      G_FILE_NAME          :string  := "DataIn.dat";
      G_DATA_W             :integer := 16
   );
   port (
      clk                  :in std_logic;
      en                   :in std_logic;
      o_data_en            :out std_logic;
      ov_data              :out std_logic_vector(G_DATA_W-1 downto 0);      
      o_eof                :out std_logic
  );
end ReadDataFromFile;

architecture ReadDataFromFile_rtl of ReadDataFromFile is
  
   file fptr: text;
   
begin

GetData_proc: process

   variable file_line      :line;
   variable varstd_data    :std_logic_vector(G_DATA_W-1 downto 0); 
   
begin                                       
   
   o_data_en   <= '0';
   ov_data     <= (others => '0');
   o_eof       <= '0';
   
   file_open(fptr, G_FILE_NAME, read_mode);
   
   while (not endfile(fptr)) loop      
      wait until clk = '1';
      if en = '1' then
         readline(fptr, file_line);
         hread(file_line, varstd_data);
         ov_data     <= varstd_data;
         o_data_en   <= '1';
      else
         ov_data     <= (others => '0');
         o_data_en   <= '0';
      end if;      
   end loop;
   wait until rising_edge(clk);
   o_data_en   <= '0';
   o_eof       <= '1';   
   file_close(fptr);
   wait;
end process;
end ReadDataFromFile_rtl;