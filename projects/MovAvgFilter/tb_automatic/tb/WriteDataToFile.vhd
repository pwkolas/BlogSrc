library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;

 
entity WriteDataToFile is
   generic (
      G_FILE_NAME          :string  := "DataOut.dat";
      G_DATA_W             :integer := 16
   );
   port (
      clk                  :in std_logic;
      en                   :in std_logic;
      iv_data              :in std_logic_vector(G_DATA_W-1 downto 0);
      i_eod                :in std_logic;
      o_eof                :out std_logic
  );
end WriteDataToFile;

architecture WriteDataToFile_rtl of WriteDataToFile is
  
   file fptr: text;
   
begin

GetData_proc: process

   variable file_line      :line;
   variable varint_data    :string(1 to 4);
   
begin                                       
   
   o_eof    <= '0';
   
   file_open(fptr, G_FILE_NAME, write_mode);
   
   while i_eod = '0' loop
      wait until clk = '1';
      if en = '1' then
         hwrite(file_line, iv_data);
         writeline(fptr, file_line);      
      end if;
   end loop;
   
   wait until clk = '1';   
   o_eof <= '1';
   file_close(fptr);
   wait;
end process;
end WriteDataToFile_rtl;