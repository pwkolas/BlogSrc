clear;
addpath ../../src/

DataInLen = 10;
SampleVal = 10;
FilLen = 4;

DataIn(1:DataInLen) = SampleVal;
DataIn = DataIn';
DataOut = AvgFilter(DataIn, FilLen);