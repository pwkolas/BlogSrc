library ieee;
use ieee.std_logic_1164.all;
 
entity tb_AvgFilter is
   generic (
      G_DATA_W    :integer := 16;
      G_FIL_L     :integer :=  4
   );
end entity;

architecture tb_AvgFilter_rtl of tb_AvgFilter is

component AvgFilter is
   generic (
      G_DATA_W    :integer := 16;
      G_FIL_L     :integer :=  4
   );
   port (
      clk         :in std_logic;
      rst         :in std_logic;
      en          :in std_logic;
      iv_data     :in std_logic_vector(G_DATA_W-1 downto 0);
      ov_avg      :out std_logic_vector(G_DATA_W-1 downto 0)
   );
end component;

signal clk        :std_logic := '0';
signal rst        :std_logic := '0';
signal en         :std_logic := '0';
signal iv_data    :std_logic_vector(G_DATA_W-1 downto 0);
signal ov_avg     :std_logic_vector(G_DATA_W-1 downto 0);

constant ClkGenConst  :time := 10 ns;

begin

UUT: AvgFilter
   generic map (
      G_DATA_W => G_DATA_W,
      G_FIL_L  => G_FIL_L
   )
   port map (
      clk      => clk,
      rst      => rst,
      en       => en,
      iv_data  => iv_data,
      ov_avg   => ov_avg
);

ClockGenerator: process
begin
  clk <= '0' after ClkGenConst, '1' after 2*ClkGenConst;
  wait for 2*ClkGenConst;
end process;

run_proc: process
   begin
         rst     <= '1';
         iv_data <= (others => '0');
         en      <= '0';

         wait for 100 ns;
            rst   <= '0';

         wait for  40 ns;
            iv_data <= x"000A";
            en      <= '1';
         wait for  20 ns;
            iv_data <= x"000A";
         wait for  20 ns;
            iv_data <= x"000A";
         wait for  20 ns;
            iv_data <= x"000A";
         wait for  20 ns;
            iv_data <= x"000A";
         wait for  20 ns;
            iv_data <= x"000A";
         wait for  20 ns;
            iv_data <= x"000A";
         wait for  20 ns;
            iv_data <= x"000A";
         wait for  20 ns;
            iv_data <= x"000A";
         wait for  20 ns;
            iv_data <= x"000A";
         wait for  20 ns;
         en    <= '0';
         wait;
   end process;
end architecture;
