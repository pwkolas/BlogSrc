----------------------------------------------------------------------------------------------------
-- Component   : tb_squareRoot_IP
-- Author      : pwkolas
----------------------------------------------------------------------------------------------------
-- File        : tb_squareRoot_IP.vhd
-- Mod. Date   : XX.XX.XXXX
-- Version     : 1.00
----------------------------------------------------------------------------------------------------
-- Description : tb for square root IP cores from Quartus Intel
--
--
----------------------------------------------------------------------------------------------------
-- Modification History :
--
----------------------------------------------------------------------------------------------------
-- Comments :
--
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_squareRoot_IP is
   generic (
      G_DATA_W : integer := 32
      );
end entity tb_squareRoot_IP;

architecture tb_squareRoot_IP_rtl of tb_squareRoot_IP is

   component ALTSQRT_in32_d0
      port (
         radical   : in  std_logic_vector (31 downto 0);
         q         : out std_logic_vector (15 downto 0);
         remainder : out std_logic_vector (16 downto 0)
         );
   end component;

   component ALTSQRT_in32_d16
      port (
         clk       : in  std_logic;
         radical   : in  std_logic_vector (31 downto 0);
         q         : out std_logic_vector (15 downto 0);
         remainder : out std_logic_vector (16 downto 0)
         );
   end component;

   constant C_CLK : time := 10 ns;

   signal clk        : std_logic := '0';
   signal radical    : std_logic_vector(G_DATA_W-1 downto 0);
   signal q_sqrt_IP1 : std_logic_vector (15 downto 0);
   signal r_sqrt_IP1 : std_logic_vector (16 downto 0);
   signal q_sqrt_IP2 : std_logic_vector (15 downto 0);
   signal r_sqrt_IP2 : std_logic_vector (16 downto 0);

begin

   ClockGenerator : process
   begin
      clk <= '0' after C_CLK, '1' after 2*C_CLK;
      wait for 2*C_CLK;
   end process;

   -- delay 0 cycles
   sqrt_IP1_UUT_inst : ALTSQRT_in32_d0
      port map (
         radical   => radical,
         q         => q_sqrt_IP1,
         remainder => r_sqrt_IP1
         );

   -- delay 16 cycles
   sqrt_IP2_UUT_inst : ALTSQRT_in32_d16
      port map (
         clk       => clk,
         radical   => radical,
         q         => q_sqrt_IP2,
         remainder => r_sqrt_IP2
         );

   run_proc : process
   begin
      wait for 100 ns;
      radical <= (others => '0');

      wait for 20 ns;
      radical <= std_logic_vector(to_unsigned(66_049, G_DATA_W));  -- 257x257

      wait for 20 ns;
      radical <= std_logic_vector(to_unsigned(328_329, G_DATA_W));  -- 573x573

      wait for 20 ns;
      radical <= std_logic_vector(to_unsigned(144, G_DATA_W));  --   12x12

      wait for 20 ns;
      radical <= std_logic_vector(to_unsigned(180, G_DATA_W));  --

      wait for 20 ns;
      radical <= std_logic_vector(to_unsigned(66_500, G_DATA_W));  --

      wait;
   end process;

end architecture tb_squareRoot_IP_rtl;
