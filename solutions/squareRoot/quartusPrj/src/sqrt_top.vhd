library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sqrt_top is
   port (
      clk      : in  std_logic;
      rst      : in  std_logic;
      startCal : in  std_logic;
      endCal   : out std_logic;

      radical : in std_logic_vector (31 downto 0);

      q_sqrt_IP1  : out std_logic_vector (15 downto 0);
      r_sqrt_IP1  : out std_logic_vector (16 downto 0);
      q_sqrt_IP2  : out std_logic_vector (15 downto 0);
      r_sqrt_IP2  : out std_logic_vector (16 downto 0);
      q_sqrt_iter : out std_logic_vector (15 downto 0);
      q_sqrt_pipe : out std_logic_vector (15 downto 0)
      );
end entity sqrt_top;

architecture sqrt_top_rtl of sqrt_top is

   component ALTSQRT_in32_d0
      port (
         radical   : in  std_logic_vector (31 downto 0);
         q         : out std_logic_vector (15 downto 0);
         remainder : out std_logic_vector (16 downto 0)
         );
   end component;

   component ALTSQRT_in32_d16
      port (
         clk       : in  std_logic;
         radical   : in  std_logic_vector (31 downto 0);
         q         : out std_logic_vector (15 downto 0);
         remainder : out std_logic_vector (16 downto 0)
         );
   end component;

   component squareRoot_iter is
      generic (
         G_DATA_W : integer := 32
         );
      port (
         clk        : in  std_logic;
         rst        : in  std_logic;
         i_startCal : in  std_logic;
         iv_data    : in  std_logic_vector(G_DATA_W-1 downto 0);
         o_endCal   : out std_logic;
         ov_res     : out std_logic_vector((G_DATA_W/2)-1 downto 0)
         );
   end component;

   component squareRoot_pipe is
      generic (
         G_DATA_W : integer := 32
         );
      port (
         clk     : in  std_logic;
         rst     : in  std_logic;
         iv_data : in  std_logic_vector(G_DATA_W-1 downto 0);
         ov_res  : out std_logic_vector((G_DATA_W/2)-1 downto 0)
         );
   end component;

begin

   -- delay 0 cycles
   sqrt_IP1_inst : ALTSQRT_in32_d0
      port map (
         radical   => radical,
         q         => q_sqrt_IP1,
         remainder => r_sqrt_IP1
         );

   -- delay 16 cycles
   sqrt_IP2_inst : ALTSQRT_in32_d16
      port map (
         clk       => clk,
         radical   => radical,
         q         => q_sqrt_IP2,
         remainder => r_sqrt_IP2
         );

   sqrt_iter_inst : squareRoot_iter
      generic map (
         G_DATA_W => 32
         )
      port map(
         clk        => clk,
         rst        => rst,
         i_startCal => startCal,
         iv_data    => radical,
         o_endCal   => endCal,
         ov_res     => q_sqrt_iter
         );

   sqrt_pipe_inst : squareRoot_pipe
      generic map (
         G_DATA_W => 32
         )
      port map(
         clk     => clk,
         rst     => rst,
         iv_data => radical,
         ov_res  => q_sqrt_pipe
         );

end architecture sqrt_top_rtl;
