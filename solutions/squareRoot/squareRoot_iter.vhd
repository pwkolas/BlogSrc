----------------------------------------------------------------------------------------------------
-- Component   : squareRoot
-- Author      : pwkolas
----------------------------------------------------------------------------------------------------
-- File        : squareRoot.vhd
-- Mod. Date   : XX.XX.XXXX
-- Version     : 1.00
----------------------------------------------------------------------------------------------------
-- Description : Square root calculator.
--               Based on
--               "A New Non-Restoring Square Root Algorithm and Its VLSI Implementations"
--
----------------------------------------------------------------------------------------------------
-- Modification History :
--
----------------------------------------------------------------------------------------------------
-- Comments :
--
----------------------------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity squareRoot_iter is
   generic (
      G_DATA_W : integer := 32
      );
   port (
      clk        : in  std_logic;
      rst        : in  std_logic;
      i_startCal : in  std_logic;
      iv_data    : in  std_logic_vector(G_DATA_W-1 downto 0);
      o_endCal   : out std_logic;
      ov_res     : out std_logic_vector((G_DATA_W/2)-1 downto 0)
      );
end entity squareRoot_iter;

architecture squareRoot_iter_rtl of squareRoot_iter is

   -- calculate size of vector needed to store given positive value
   function log2(m : positive) return natural is
   begin
      for index in 1 to 30 loop
         if(m < 2**index) then
            return(index);
         end if;
      end loop;
      return(31);
   end function;

   constant C_ALU_W     : integer := ((G_DATA_W/2) + 2);
   constant C_CLKCYC_NR : integer := G_DATA_W/2;

   signal data : unsigned(G_DATA_W-1 downto 0);  -- (D)

   signal aluInLoopR : unsigned(C_ALU_W-1 downto 0);  -- (ALU R in)
   signal aluInQ     : unsigned(C_ALU_W-1 downto 0);  -- (ALU Q in)
   signal nextOp     : std_logic;

   signal endCal : std_logic;

   signal cyc_cnt : unsigned(log2(G_DATA_W/2)-1 downto 0);

begin
   sqrt_p : process (clk, rst)
      variable v_resAlu_tmp : unsigned(C_ALU_W-1 downto 0);  --(R)
   begin
      if (rst = '1') then
         data         <= (others => '0');
         aluInLoopR   <= (others => '0');
         aluInQ       <= (others => '0');
         v_resAlu_tmp := (others => '0');
         nextOp       <= '0';
         endCal       <= '0';
         cyc_cnt      <= (others => '0');
      elsif rising_edge(clk) then
         -- start conditions
         if (i_startCal = '1') then
            data                   <= shift_left(unsigned(iv_data), 2);
            aluInLoopR             <= (others => '0');
            aluInLoopR(1 downto 0) <= unsigned(iv_data(G_DATA_W-1 downto G_DATA_W-1-1));
            aluInQ                 <= (others => '0');
            aluInQ(0)              <= '1';
            v_resAlu_tmp           := (others => '0');
            nextOp                 <= '0';
            endCal                 <= '0';
            cyc_cnt                <= to_unsigned(C_CLKCYC_NR, cyc_cnt'length);
         end if;

         if cyc_cnt > 0 then
            if (nextOp = '1') then
               v_resAlu_tmp := aluInLoopR + aluInQ;
            else
               v_resAlu_tmp := aluInLoopR - aluInQ;
            end if;
            cyc_cnt <= cyc_cnt - 1;

            -- update registers
            data                           <= shift_left(data, 2);
            aluInLoopR(C_ALU_W-1 downto 2) <= v_resAlu_tmp(C_ALU_W-2-1 downto 0);
            aluInLoopR(1 downto 0)         <= data(G_DATA_W-1 downto G_DATA_W-1-1);
            aluInQ(C_ALU_W-1 downto 2)     <= shift_left(aluInQ(C_ALU_W-1 downto 2), 1);
            aluInQ(2)                      <= not v_resAlu_tmp(v_resAlu_tmp'high);
            aluInQ(1)                      <= v_resAlu_tmp(v_resAlu_tmp'high);
            nextOp                         <= v_resAlu_tmp(v_resAlu_tmp'high);
         end if;

         if (cyc_cnt = 1) then
            endCal <= '1';
         else
            endCal <= '0';
         end if;

      end if;
   end process;

   ov_res   <= std_logic_vector(aluInQ(C_ALU_W-1 downto 2));
   o_endCal <= endCal;

end architecture squareRoot_iter_rtl;
