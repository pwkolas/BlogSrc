----------------------------------------------------------------------------------------------------
-- Component   : tb_squareRoot
-- Author      : pwkolas
----------------------------------------------------------------------------------------------------
-- File        : tb_squareRoot.vhd
-- Mod. Date   : XX.XX.XXXX
-- Version     : 1.00
----------------------------------------------------------------------------------------------------
-- Description : tb for square root calculator.
--               Based on
--               "A New Non-Restoring Square Root Algorithm and Its VLSI Implementations"
--
----------------------------------------------------------------------------------------------------
-- Modification History :
--
----------------------------------------------------------------------------------------------------
-- Comments :
--
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_squareRoot_iter is
   generic (
      G_DATA_W : integer := 32
      );
end entity tb_squareRoot_iter;

architecture tb_squareRoot_iter_rtl of tb_squareRoot_iter is

   component squareRoot_iter is
      generic (
         G_DATA_W : integer := 32
         );
      port (
         clk        : in  std_logic;
         rst        : in  std_logic;
         i_startCal : in  std_logic;
         iv_data    : in  std_logic_vector(G_DATA_W-1 downto 0);
         o_endCal   : out std_logic;
         ov_res     : out std_logic_vector((G_DATA_W/2)-1 downto 0)
         );
   end component;

   constant C_CLK : time := 10 ns;

   signal clk      : std_logic := '0';
   signal rst      : std_logic;
   signal startCal : std_logic;
   signal data     : std_logic_vector(G_DATA_W-1 downto 0);
   signal endCal   : std_logic;
   signal res      : std_logic_vector((G_DATA_W/2)-1 downto 0);

begin

   ClockGenerator : process
   begin
      clk <= '0' after C_CLK, '1' after 2*C_CLK;
      wait for 2*C_CLK;
   end process;

   UUT : squareRoot_iter
      generic map (
         G_DATA_W => G_DATA_W
         )
      port map (
         clk        => clk,
         rst        => rst,
         i_startCal => startCal,
         iv_data    => data,
         o_endCal   => endCal,
         ov_res     => res
         );

   rst <= '1', '0' after 100 ns;

   startCal <= '0',
               '1' after 200 ns, '0' after 220 ns,
               '1' after 1200 ns, '0' after 1220 ns,
               '1' after 2200 ns, '0' after 2220 ns,
               '1' after 3200 ns, '0' after 3220 ns,
               '1' after 4200 ns, '0' after 4220 ns;

   data <= std_logic_vector(to_unsigned(66_049, G_DATA_W)),  -- 257x257
           std_logic_vector(to_unsigned(328_329, G_DATA_W)) after 1 us,  -- 573x573
           std_logic_vector(to_unsigned(144, G_DATA_W))     after 2 us,  --   12x12
           std_logic_vector(to_unsigned(180, G_DATA_W))     after 3 us,  --
           std_logic_vector(to_unsigned(66_500, G_DATA_W))  after 4 us;  -- 

end architecture tb_squareRoot_iter_rtl;
