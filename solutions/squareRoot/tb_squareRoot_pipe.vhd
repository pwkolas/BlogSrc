----------------------------------------------------------------------------------------------------
-- Component   : tb_squareRoot_pipe
-- Author      : pwkolas
----------------------------------------------------------------------------------------------------
-- File        : tb_squareRoot.vhd
-- Mod. Date   : XX.XX.XXXX
-- Version     : 1.00
----------------------------------------------------------------------------------------------------
-- Description : tb for square root calculator.
--               Based on
--               "A New Non-Restoring Square Root Algorithm and Its VLSI Implementations"
--
----------------------------------------------------------------------------------------------------
-- Modification History :
--
----------------------------------------------------------------------------------------------------
-- Comments :
--
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_squareRoot_pipe is
   generic (
      G_DATA_W : integer := 32
      );
end entity tb_squareRoot_pipe;

architecture tb_squareRoot_pipe_rtl of tb_squareRoot_pipe is

   component squareRoot_pipe is
      generic (
         G_DATA_W : integer := 32
         );
      port (
         clk     : in  std_logic;
         rst     : in  std_logic;
         iv_data : in  std_logic_vector(G_DATA_W-1 downto 0);
         ov_res  : out std_logic_vector((G_DATA_W/2)-1 downto 0)
         );
   end component;

   constant C_CLK : time := 10 ns;

   signal clk  : std_logic := '0';
   signal rst  : std_logic;
   signal data : std_logic_vector(G_DATA_W-1 downto 0);
   signal res  : std_logic_vector((G_DATA_W/2)-1 downto 0);

begin

   ClockGenerator : process
   begin
      clk <= '0' after C_CLK, '1' after 2*C_CLK;
      wait for 2*C_CLK;
   end process;

   UUT : squareRoot_pipe
      generic map (
         G_DATA_W => G_DATA_W
         )
      port map (
         clk     => clk,
         rst     => rst,
         iv_data => data,
         ov_res  => res
         );

   rst <= '1', '0' after 100 ns;

   run_proc : process
   begin
      data <= (others => '0');

      wait for 500 ns;
      data <= std_logic_vector(to_unsigned(66_049, G_DATA_W));  -- 257x257

      wait for 20 ns;
      data <= std_logic_vector(to_unsigned(328_329, G_DATA_W));  -- 573x573

      wait for 20 ns;
      data <= std_logic_vector(to_unsigned(144, G_DATA_W));  --   12x12

      wait for 20 ns;
      data <= std_logic_vector(to_unsigned(180, G_DATA_W));  --

      wait for 20 ns;
      data <= std_logic_vector(to_unsigned(66_500, G_DATA_W));  --

      wait;
   end process;

end architecture tb_squareRoot_pipe_rtl;
